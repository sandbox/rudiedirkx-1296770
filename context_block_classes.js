
jQuery(function() {
	var $ = jQuery

	$('a.content-block-classes[data-for]').click(function(e) {
		e.preventDefault()

		var el_id = $(this).data('for')
		var el = $('#' + el_id)[0]
		if ( el ) {
			var new_classes = prompt('Classes for this block in this location', String(el.value))
			if ( null != new_classes ) {
				el.value = new_classes
			}
		}
	})
})
